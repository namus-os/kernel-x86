NAMUS OS x86 - WIP
==================
What?
-----
Experimental OS. Only tested under QEMU on linux for now.

Why?
----
Why not?

<!--Screenshot
----------
![Screenshot](https://i.imgur.com/noQetgM.png)
(Yes that is all for now)

How? (Cloning and running)
-------
First off, you need to get all submodules (like new qemu), so call git clone with the recursive flag

    git clone --recursive https://gitlab.com/Wazzaps/namus-os.git

next compile a stable qemu version

    cd qemu
    git checkout v2.10.0
    git submodule update --init --recursive
    ./configure
    make

now you can just run the os (double ctrl+c to exit)

    cd ..
    ./run.sh

and for development

    ./repl.sh

which also executes `make clean` and `make` between runs
-->